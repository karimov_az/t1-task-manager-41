package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.enumerated.TaskSort;
import ru.t1.karimov.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    Task add(@NotNull Task model) throws Exception;

    @NotNull
    Task changeTaskStatusById (
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    Task changeTaskStatusByIndex (
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    Task create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    List<Task> findAll() throws Exception;

    @NotNull
    List<Task> findAll(@Nullable String userId) throws Exception;

    @NotNull
    List<Task> findAll(@Nullable String userId, @Nullable TaskSort sort) throws Exception;

    @Nullable
    Task findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    void removeAll() throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    void removeOne(@Nullable String userId, @Nullable Task model) throws Exception;

    void removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    void removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    void set(@NotNull Collection<Task> tasks) throws Exception;

    @NotNull
    Task updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @NotNull
    Task updateTaskByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

}
