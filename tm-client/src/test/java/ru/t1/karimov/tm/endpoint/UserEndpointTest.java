package ru.t1.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.karimov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.karimov.tm.api.endpoint.IUserEndpoint;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.dto.request.user.*;
import ru.t1.karimov.tm.dto.response.user.*;
import ru.t1.karimov.tm.marker.SoapCategory;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.service.PropertyService;

import static org.junit.Assert.*;

@Category(SoapCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @Nullable
    private String adminToken;

    @Nullable
    private String testToken;

    @Nullable
    private UserRegistryResponse tomRegistryResponse;

    @Before
    public void initTest() throws Exception {
        @NotNull final UserLoginResponse adminResponse = authEndpoint.login(new UserLoginRequest("admin", "admin"));
        adminToken = adminResponse.getToken();
        @NotNull final UserLoginResponse testResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        testToken = testResponse.getToken();

        @NotNull final UserRegistryRequest adminRegistryRequest = new UserRegistryRequest(adminToken);
        adminRegistryRequest.setLogin("tom");
        adminRegistryRequest.setPassword("tom");
        adminRegistryRequest.setEmail("tom@tst.ru");
        tomRegistryResponse = userEndpoint.registryUser(adminRegistryRequest);
    }

    @After
    public void initEndTest() throws Exception {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken);
        removeRequest.setLogin("tom");
        assertNotNull(userEndpoint.removeUser(removeRequest));

        @NotNull final UserLogoutRequest adminRequest = new UserLogoutRequest(adminToken);
        @NotNull final UserLogoutRequest testRequest = new UserLogoutRequest(testToken);
        authEndpoint.logout(adminRequest);
        authEndpoint.logout(testRequest);
    }

    @Test
    public void testChangePassword() throws Exception {
        assertNotNull(testToken);
        @NotNull final UserChangePasswordRequest changePasswordRequest = new UserChangePasswordRequest();
        changePasswordRequest.setToken(testToken);
        changePasswordRequest.setPassword("newPassword");
        @NotNull final UserChangePasswordResponse changePasswordResponse = userEndpoint.changeUserPassword(
                changePasswordRequest
        );
        assertNotNull(changePasswordResponse);
        assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", "test"))
        );
        changePasswordRequest.setPassword("test");
        @NotNull final UserChangePasswordResponse changePasswordBackResponse = userEndpoint.changeUserPassword(
                changePasswordRequest
        );
        assertNotNull(changePasswordBackResponse);
    }

    @Test
    public void testLockUnlock() throws Exception {
        assertNotNull(testToken);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(testToken);
        assertNotNull(authEndpoint.logout(logoutRequest));

        @NotNull final UserLockRequest lockRequest = new UserLockRequest(adminToken);
        lockRequest.setLogin("test");
        assertNotNull(userEndpoint.lockUser(lockRequest));
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest("test", "test");

        assertThrows(
                Exception.class,
                () -> authEndpoint.login(userLoginRequest)
        );
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(adminToken);
        unlockRequest.setLogin("test");
        userEndpoint.unlockUser(unlockRequest);

        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(userLoginRequest);
        @Nullable final String token = userLoginResponse.getToken();
        assertNotNull(token);
        testToken = token;
    }

    @Test
    public void testRegistryUser() throws Exception {
        @NotNull final UserRegistryRequest adminRegistryRequest = new UserRegistryRequest(adminToken);
        adminRegistryRequest.setLogin("tomas");
        adminRegistryRequest.setPassword("tomas");
        adminRegistryRequest.setEmail("tomas@tst.ru");
        @NotNull final UserRegistryResponse response = userEndpoint.registryUser(adminRegistryRequest);
        assertNotNull(response);
        @Nullable final User user = response.getUser();
        assertNotNull(user);
        @Nullable final String userLogin = user.getLogin();
        assertNotNull(userLogin);
        assertEquals(adminRegistryRequest.getLogin(), userLogin);

        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken);
        removeRequest.setLogin("tomas");
        @NotNull final UserRemoveResponse removeResponse = userEndpoint.removeUser(removeRequest);
        @Nullable final User removeUser = removeResponse.getUser();
        assertNotNull(removeUser);
    }

    @Test
    public void testRemoveUser() throws Exception {
        assertNotNull(tomRegistryResponse);
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken);
        removeRequest.setLogin("tom");
        assertNotNull(userEndpoint.removeUser(removeRequest));

        assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("tom", "tom"))
        );

        @NotNull final UserRegistryRequest adminRegistryRequest = new UserRegistryRequest(adminToken);
        adminRegistryRequest.setLogin("tom");
        adminRegistryRequest.setPassword("tom");
        adminRegistryRequest.setEmail("tom@tst.ru");
        tomRegistryResponse = userEndpoint.registryUser(adminRegistryRequest);
    }

    @Test
    public void testUpdateUserProfile() throws Exception {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest("tom", "tom");
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(userLoginRequest);
        @Nullable final String token = userLoginResponse.getToken();
        assertNotNull(token);

        @NotNull final UserProfileResponse userProfileResponse = authEndpoint.profile(new UserProfileRequest(token));
        @Nullable User user = userProfileResponse.getUser();
        assertNotNull(user);

        @NotNull final UserUpdateProfileRequest updateProfileRequest = new UserUpdateProfileRequest(token);
        updateProfileRequest.setFirstName("firstName");
        updateProfileRequest.setLastName("lastName");
        updateProfileRequest.setMiddleName("middleName");
        @NotNull final UserUpdateProfileResponse updateProfileResponse = userEndpoint.updateUserProfile(
                updateProfileRequest
        );
        user = updateProfileResponse.getUser();
        assertNotNull(user);
        assertEquals("lastName", user.getLastName());
        assertEquals("firstName", user.getFirstName());
        assertEquals("middleName", user.getMiddleName());

        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(token);
        authEndpoint.logout(logoutRequest);
    }

}
